//Problem 1: Iterate over each object in the array and in the email breakdown the email and return the output as below:
/*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */

const getDetailsFromEmail = (dataSet) => {
    if (Array.isArray(dataSet)) {
        return dataSet.map((person) => {
            let emailParts = person.email.split("@");
            let [firstName, lastName] = emailParts[0].split(".");
            return {
                firstName: firstName.charAt(0).toUpperCase() + firstName.slice(1),
                lastName: lastName.charAt(0).toUpperCase() + lastName.slice(1),
                emailDomain: emailParts[1],
            };
        });
    }
    else {
        return [];
    }
};

module.exports = getDetailsFromEmail;