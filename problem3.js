//Problem 3: From the given data, filter all the unique langauges and return in an array

const getAllUniqueLanguages = (dataSet) => {
    if (Array.isArray(dataSet)) {
        const allLanguages = dataSet.flatMap((person) => person.languages);
        return Array.from(new Set(allLanguages));
    }
    else{
        return [];
    }
};

module.exports = getAllUniqueLanguages;