//Problem 2: Check the each person projects list and group the data based on the project status and return the data as below

/*
 
  {
    'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
    'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
    .. so on
  }
 
*/


const groupProjectsByStatus = (dataSet) => {
    if (Array.isArray(dataSet)) {
        return dataSet.reduce((groupedProjects, person) => {
            person.projects.forEach((project) => {
                if (!groupedProjects[project.status]) {
                    groupedProjects[project.status] = [];
                }
                groupedProjects[project.status].push(project.name);
            });
            return groupedProjects;
        }, {});
    }
    else{
        return [];
    }
};

module.exports = groupProjectsByStatus;
