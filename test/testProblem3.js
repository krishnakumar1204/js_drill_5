const dataSet = require("../data");
const getAllUniqueLanguages = require("../problem3");

try {
    console.log(getAllUniqueLanguages(dataSet));
} catch (error) {
    console.log("Something went wrong");
}